package com.shtreb.tracker.entities;

public class GeoPoint {
    public double Latitude;
    public double Longitude;

    public GeoPoint(double latitude, double longitude) {
        Latitude = latitude;
        Longitude = longitude;
    }
}
