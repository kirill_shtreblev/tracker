package com.shtreb.tracker.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GeoLocation {
    @Expose
    @SerializedName("lat")
    private double lat;

    @Expose
    @SerializedName("lon")
    private double lon;

    @Expose
    @SerializedName("speed")
    private double speed;

    @Expose
    @SerializedName("azimut")
    private double azimut;

    public GeoLocation(){}

    public double getLat() {
        return lat;
    }

    public GeoLocation setLat(double lat) {
        this.lat = lat;
        return this;
    }

    public double getLon() {
        return lon;
    }

    public GeoLocation setLon(double lon) {
        this.lon = lon;
        return this;
    }

    public double getSpeed() {
        return speed;
    }

    public GeoLocation setSpeed(double speed) {
        this.speed = speed;
        return this;
    }

    public double getAzimut() {
        return azimut;
    }

    public GeoLocation setAzimut(double azimut) {
        this.azimut = azimut;
        return this;
    }
}
