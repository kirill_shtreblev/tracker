package com.shtreb.tracker.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Provider {

    public static final String PROVIDER_GPS = "GPS";
    public static final String PROVIDER_KALMAN = "Kalman";
    public static final String PROVIDER_GEOHASH = "GeoHash";

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("track")
    private List<GeoLocation> track;

    public Provider(String name){
        this.name = name;
        track = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<GeoLocation> getTrack() {
        return track;
    }

    public void setTrack(List<GeoLocation> track) {
        this.track = track;
    }

    public boolean addTrack(GeoLocation geolocation){
        if(track == null)
            track = new ArrayList<>();

        if(track.isEmpty()) {
            track.add(geolocation);
            return true;
        }

        GeoLocation last = track.get(track.size()-1);

        if(last.getLat() == geolocation.getLat() && last.getLon() == geolocation.getLon())
            return false;

        if(!track.contains(geolocation)) {
            track.add(geolocation);
            return true;
        }

        return false;
    }
}
