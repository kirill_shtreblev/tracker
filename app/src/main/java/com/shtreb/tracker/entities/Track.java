package com.shtreb.tracker.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Track {

    @Expose
    @SerializedName("GPS")
    private Provider GPS;

    @Expose
    @SerializedName("Kalman")
    private Provider Kalman;

    @Expose
    @SerializedName("GeoHash")
    private Provider GeoHash;

    public Track() {
        GPS = new Provider(Provider.PROVIDER_GPS);
        Kalman = new Provider(Provider.PROVIDER_KALMAN);
        GeoHash = new Provider(Provider.PROVIDER_GEOHASH);
    }

    public Provider getGPS() {
        return GPS;
    }

    public void setGPS(Provider GPS) {
        this.GPS = GPS;
    }

    public Provider getKalman() {
        return Kalman;
    }

    public void setKalman(Provider kalman) {
        Kalman = kalman;
    }

    public Provider getGeoHash() {
        return GeoHash;
    }

    public void setGeoHash(Provider geoHash) {
        GeoHash = geoHash;
    }
}
