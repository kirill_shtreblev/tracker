package com.shtreb.tracker.views.fragments;

import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.shtreb.tracker.R;
import com.shtreb.tracker.databinding.FragmentBottomPanelBinding;
import com.shtreb.tracker.entities.Provider;
import com.shtreb.tracker.views.fragments.interfaces.BottomPanelFragmentInterface;

public class BottomPanelFragment extends Fragment implements BottomPanelFragmentInterface.BottomPanelCallBack, CompoundButton.OnCheckedChangeListener {
    public static final String LOG_TAG = BottomPanelFragment.class.getName();

    private FragmentBottomPanelBinding mBinding;
    private BottomPanelFragmentInterface.ActivityBottomPanel mActivityCallBack;

    private Location mGPS, mKalman;
    private double mSpeed1, mSpeed2;
    private float mBearing1, mBearing2;

    private boolean isVisible = true;

    public static BottomPanelFragment newIntent(){
        return new BottomPanelFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        mBinding = (FragmentBottomPanelBinding) DataBindingUtil.inflate(inflater, R.layout.fragment_bottom_panel, container, false);

        setupView();

        return mBinding.getRoot();
    }

    private void setupView(){
        mActivityCallBack = (BottomPanelFragmentInterface.ActivityBottomPanel) getActivity();
        mBinding.rlDrag.setOnClickListener(cl -> {
            isVisible = !isVisible;
            mActivityCallBack.onChangeVisibility(isVisible);
        });
        mBinding.btnSave.setOnClickListener(cl -> mActivityCallBack.saveToFile());

        mBinding.cbGps.setOnCheckedChangeListener(this);
        mBinding.chKalman.setOnCheckedChangeListener(this);
        mBinding.chGeohash.setOnCheckedChangeListener(this);
    }

    @Override
    public void setLocation(Location location, String provider) {

        if(provider.equals(Provider.PROVIDER_GEOHASH))
            return;

        if(provider.equals(Provider.PROVIDER_KALMAN)) {
            mKalman = location;
            mSpeed1 = (double)Math.round(mKalman.getSpeed()*100*3.6)/100;
            mBearing1 = mKalman.getBearing();
        }
        if(provider.equals(Provider.PROVIDER_GPS)) {
            mGPS = location;
            mSpeed2 = (double) Math.round(mGPS.getSpeed() * 100 * 3.6) / 100;
            mBearing2 = mGPS.getBearing();
        }

        if(mGPS == null || mKalman == null)
            return;

        mBinding.tvSpeed.setText(mSpeed1+" ("+mSpeed2+")");
        mBinding.tvAngle.setText(mBearing1+" ("+mBearing2+")");
        mBinding.tvPosition.setText("Lat: "+mKalman.getLatitude()+" Lon: "+mKalman.getLongitude());
    }

    @Override
    public void onCheckedChanged(CompoundButton cb, boolean isChecked) {
        mActivityCallBack.onChangeCheckBox(new boolean[]{mBinding.cbGps.isChecked(), mBinding.chKalman.isChecked(), mBinding.chGeohash.isChecked()});
    }
}
