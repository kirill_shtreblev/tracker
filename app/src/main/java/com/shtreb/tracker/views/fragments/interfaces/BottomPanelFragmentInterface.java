package com.shtreb.tracker.views.fragments.interfaces;

import android.location.Location;

public interface BottomPanelFragmentInterface {
    public interface ActivityBottomPanel{
        void saveToFile();

        void onChangeVisibility(boolean isVisible);

        void onChangeCheckBox(boolean[] provider);
    }

    public interface BottomPanelCallBack{
        void setLocation(Location location, String provider);
    }
}
