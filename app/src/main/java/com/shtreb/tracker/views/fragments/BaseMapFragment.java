package com.shtreb.tracker.views.fragments;

import android.location.Location;
import android.os.Bundle;
import android.os.Trace;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.shtreb.tracker.R;
import com.shtreb.tracker.cases.FileManager;
import com.shtreb.tracker.cases.GeoHashRTFilter;
import com.shtreb.tracker.cases.LocationListener;
import com.shtreb.tracker.entities.GeoLocation;
import com.shtreb.tracker.entities.Provider;
import com.shtreb.tracker.entities.Track;

import java.util.List;

public class BaseMapFragment extends Fragment implements OnMapReadyCallback{
    public static final String LOG_TAG = BaseMapFragment.class.getName();

    private static final float POLYLINE_WIDTH = 8;

    protected GoogleMap mMap;

    protected Marker mLocation;
    protected Circle mAccuracy;

    protected GeoHashRTFilter mGeoHashFilter;

    protected Polyline mPolylineGPS,
            mPolylineKalman,
            mPolylineKalmanGeoHash;

    protected List<LatLng> mListPointsGPS,
            mListPointsKalman,
            mListPointsGeoHash;

    protected Gson mGson;
    protected Track mTrack;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGeoHashFilter = new GeoHashRTFilter(8, 2);
    }

    protected void createMap(){
        SupportMapFragment mapFr = SupportMapFragment.newInstance();
        FragmentTransaction frTransaction = getChildFragmentManager().beginTransaction();
        frTransaction.replace(R.id.fl_map, mapFr).commitAllowingStateLoss();
        mapFr.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        onMapInitialized();
    }

    protected void onMapInitialized(){}

    protected void onLocationChanged(Location location, String provider){
        if(location == null)
            return;

        if(mLocation == null) createLocationMarker(location);
        else if(provider.equals(Provider.PROVIDER_GEOHASH)){setChangeLocation(location);}

        setChangePolyline(location, provider);
    }

    private void createLocationMarker(Location location){
        MarkerOptions mOptionsLocation = new MarkerOptions();
        mOptionsLocation.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_my_location));
        mOptionsLocation.position(new LatLng(location.getLatitude(), location.getLongitude()));
        mOptionsLocation.anchor(.5f, .5f);

        CircleOptions mOptionsAccuracy = new CircleOptions();
        mOptionsAccuracy.center(new LatLng(location.getLatitude(), location.getLongitude()));
        mOptionsAccuracy.radius(location.getAccuracy());
        mOptionsAccuracy.fillColor(ContextCompat.getColor(getActivity(), R.color.colorCircleAccuracy));
        mOptionsAccuracy.strokeWidth(.0f);

        mLocation = mMap.addMarker(mOptionsLocation);
        mAccuracy = mMap.addCircle(mOptionsAccuracy);
    }

    private void setChangeLocation(Location location){
        LatLng center = new LatLng(location.getLatitude(), location.getLongitude());
        mLocation.setPosition(center);
        mAccuracy.setCenter(center);
        mAccuracy.setRadius(location.getAccuracy());
    }

    private void createPolyline(){
        PolylineOptions optGPS, optKalman, optKalmanGeoHash;

        optGPS = new PolylineOptions().color(ContextCompat.getColor(getActivity(), R.color.colorPolylineGPS)).width(POLYLINE_WIDTH);
        optKalman = new PolylineOptions().color(ContextCompat.getColor(getActivity(), R.color.colorPolylineKalman)).width(POLYLINE_WIDTH);
        optKalmanGeoHash = new PolylineOptions().color(ContextCompat.getColor(getActivity(), R.color.colorPolylineKalmanGeoHash)).width(POLYLINE_WIDTH);

        mPolylineGPS           = mMap.addPolyline(optGPS);
        mPolylineKalman        = mMap.addPolyline(optKalman);
        mPolylineKalmanGeoHash = mMap.addPolyline(optKalmanGeoHash);

        mListPointsGPS = mPolylineGPS.getPoints();
        mListPointsKalman = mPolylineKalman.getPoints();
        mListPointsGeoHash = mPolylineKalmanGeoHash.getPoints();
    }

    private void setChangePolyline(Location location, String provider){
        if(mPolylineGPS == null || mPolylineKalman == null || mPolylineKalmanGeoHash == null)
            createPolyline();

        GeoLocation geoLocation = new GeoLocation().setLat(location.getLatitude()).setLon(location.getLongitude())
                .setSpeed(location.getSpeed()).setAzimut(location.getBearing());

        switch (provider){
            case LocationListener.PROVIDER_GPS:
                if(mTrack.getGPS().addTrack(geoLocation)){
                    mListPointsGPS.add(new LatLng(geoLocation.getLat(), geoLocation.getLon()));
                    mPolylineGPS.setPoints(mListPointsGPS);
                }
                break;
            case LocationListener.PROVIDER_KALMAN:
                if(mTrack.getGeoHash().addTrack(geoLocation)){
                    mListPointsKalman.add(new LatLng(geoLocation.getLat(), geoLocation.getLon()));
                    mPolylineKalman.setPoints(mListPointsKalman);
                }
                break;
            case LocationListener.PROVIDER_KALMAN_GEOHASH:
                if(mTrack.getGeoHash().addTrack(geoLocation)){
                    mListPointsGeoHash.add(new LatLng(geoLocation.getLat(), geoLocation.getLon()));
                    mPolylineKalmanGeoHash.setPoints(mListPointsGeoHash);
                }
                break;
            default:
                break;
        }

        if(mPolylineKalman.getPoints().isEmpty())
            return;

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for(LatLng latLng : mPolylineKalman.getPoints())
            builder.include((LatLng) latLng);

        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 0));
    }

    private List<LatLng> checkNewPoint(List<LatLng> points, Location location){

        if(points.isEmpty())
            points.add(new LatLng(location.getLatitude(), location.getLongitude()));
        else{
            LatLng last = points.get(points.size() - 1);

            if(last.latitude == location.getLatitude() && last.longitude == location.getLongitude())
                return points;
            else
                points.add(new LatLng(location.getLatitude(), location.getLongitude()));
        }

        return points;
    }

    protected Track parseTrack(){
        return mGson.fromJson(FileManager.readLogFile(), Track.class);
    }
}
