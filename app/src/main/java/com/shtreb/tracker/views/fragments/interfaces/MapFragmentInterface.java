package com.shtreb.tracker.views.fragments.interfaces;

import android.location.Location;

public interface MapFragmentInterface {
    public interface ActivityMap{

    }

    public interface FragmentMap{
        void setLocation(Location location, String provider);
        void saveToFile();
        void changeCheckBox(boolean[] provider);
    }
}
