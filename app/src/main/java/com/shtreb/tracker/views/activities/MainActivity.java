package com.shtreb.tracker.views.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.shtreb.tracker.R;
import com.shtreb.tracker.cases.interfaces.LocationListener;
import com.shtreb.tracker.databinding.ActivityMainBinding;
import com.shtreb.tracker.entities.Track;
import com.shtreb.tracker.views.fragments.interfaces.BottomPanelFragmentInterface;
import com.shtreb.tracker.views.fragments.interfaces.MapFragmentInterface;

public class MainActivity extends BaseMainActivity implements MapFragmentInterface.ActivityMap, LocationListener.CallBack, BottomPanelFragmentInterface.ActivityBottomPanel {
    public static final String LOG_TAG = MainActivity.class.getName();

    private ActivityMainBinding mBinding;
    private LocationListener.Initializer mLocationListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = (ActivityMainBinding) DataBindingUtil.setContentView(this, R.layout.activity_main);

        setupModel();
    }

    private void setupModel(){
        createMap(mBinding.flMap);
        createBottomPanel(mBinding);

        /*mLocationListener = (LocationListener.Initializer) new com.shtreb.tracker.cases.LocationListener(this, (SensorManager) getSystemService(SENSOR_SERVICE));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            ((LocationManager) getSystemService(LOCATION_SERVICE)).requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mLocationListener.onLoadGPSLocation());*/
    }

    @Override
    public void onLocationChange(final Location location, String provider) {
        mMapCallBack.setLocation(location, provider);
        mBottomPanelCallBack.setLocation(location, provider);
    }

    @Override
    public void saveToFile() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
        mMapCallBack.saveToFile();
    }

    @Override
    public void onChangeVisibility(boolean isVisible) {
        if(isVisible)
            mBottomPanelSheetBehaviour.setState(BottomSheetBehavior.STATE_EXPANDED);
        else
            mBottomPanelSheetBehaviour.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @Override
    public void onChangeCheckBox(boolean[] provider) {
        mMapCallBack.changeCheckBox(provider);
    }
}
