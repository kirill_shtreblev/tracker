package com.shtreb.tracker.views.activities;

import android.annotation.SuppressLint;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;

import com.shtreb.tracker.R;
import com.shtreb.tracker.cases.DisableBottomSheetBehavior;
import com.shtreb.tracker.databinding.ActivityMainBinding;
import com.shtreb.tracker.views.fragments.BottomPanelFragment;
import com.shtreb.tracker.views.fragments.MapFragment;
import com.shtreb.tracker.views.fragments.interfaces.BottomPanelFragmentInterface;
import com.shtreb.tracker.views.fragments.interfaces.MapFragmentInterface;

@SuppressLint("Registered")
public class BaseMainActivity extends AppCompatActivity{
    public static String LOG_TAG = BaseMainActivity.class.getName();

    protected MapFragmentInterface.FragmentMap mMapCallBack;
    protected DisableBottomSheetBehavior<View> mBottomPanelSheetBehaviour;
    protected BottomPanelFragmentInterface.BottomPanelCallBack mBottomPanelCallBack;

    protected void createMap(FrameLayout fLayout){
        Fragment fr = MapFragment.newInstance();
        FragmentTransaction frTransaction = getSupportFragmentManager().beginTransaction();
        frTransaction.replace(fLayout.getId(), fr);
        frTransaction.commitAllowingStateLoss();

        mMapCallBack = (MapFragmentInterface.FragmentMap) fr;
    }

    protected void createBottomPanel(ActivityMainBinding binding){
        Fragment fr = BottomPanelFragment.newIntent();
        FragmentTransaction frTransaction = getSupportFragmentManager().beginTransaction();
        frTransaction.replace(binding.flBottomPanel.getId(), fr, BottomPanelFragment.LOG_TAG);
        frTransaction.commitAllowingStateLoss();

        mBottomPanelCallBack = (BottomPanelFragmentInterface.BottomPanelCallBack) fr;

        mBottomPanelSheetBehaviour = (DisableBottomSheetBehavior) BottomSheetBehavior.from(binding.flBottomPanel);
        mBottomPanelSheetBehaviour.setHideable(false);
        mBottomPanelSheetBehaviour.setPeekHeight(getResources().getDimensionPixelSize(R.dimen.bottom_panel_peek_height));
        mBottomPanelSheetBehaviour.setState(BottomSheetBehavior.STATE_EXPANDED);
        mBottomPanelSheetBehaviour.setAllowUserDragging(true);
    }
}
