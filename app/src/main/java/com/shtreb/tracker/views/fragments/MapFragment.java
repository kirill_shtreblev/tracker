package com.shtreb.tracker.views.fragments;

import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.Marker;
import com.google.gson.Gson;
import com.shtreb.tracker.R;
import com.shtreb.tracker.cases.FileManager;
import com.shtreb.tracker.databinding.FragmentMapBinding;
import com.shtreb.tracker.entities.GeoLocation;
import com.shtreb.tracker.entities.GeoPoint;
import com.shtreb.tracker.entities.Provider;
import com.shtreb.tracker.entities.Track;
import com.shtreb.tracker.views.fragments.interfaces.BottomPanelFragmentInterface;
import com.shtreb.tracker.views.fragments.interfaces.MapFragmentInterface;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class MapFragment extends BaseMapFragment implements OnMapReadyCallback, MapFragmentInterface.FragmentMap{
    public static final String LOG_TAG = MapFragment.class.getName();

    private FragmentMapBinding mBinding;
    private MapFragmentInterface.ActivityMap mActivityCallBack;

    private Marker mMarkerLocation;

    public static MapFragment newInstance(){ return new MapFragment(); }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        mBinding = (FragmentMapBinding) DataBindingUtil.inflate(inflater, R.layout.fragment_map, container, false);

        setupModel();

        return mBinding.getRoot();
    }

    private void setupModel(){
        mActivityCallBack = (MapFragmentInterface.ActivityMap) getActivity();
        mTrack = new Track();
        mGson = new Gson();
        createMap();
    }

    protected void onMapInitialized(){
        buildTrackFromFile();
    }

    private void buildTrackFromFile(){
        Track track = parseTrack();
        Track nTrack = new Track();
        try{createTrack(nTrack, track.getGPS());}catch (Exception e){e.printStackTrace();Log.e(LOG_TAG, Provider.PROVIDER_GPS+" Exception");}
        try{createTrack(nTrack, track.getKalman());}catch (Exception e){e.printStackTrace();Log.e(LOG_TAG, Provider.PROVIDER_KALMAN+" Exception");}
        try{createTrack(nTrack, track.getGeoHash());}catch (Exception e){e.printStackTrace();Log.e(LOG_TAG, Provider.PROVIDER_GEOHASH+" Exception");}

        //FileManager.saveToFile(mGson.toJson(nTrack));
    }

    private void createTrack(Track track, Provider provider){
        if(provider.getTrack() == null || provider.getTrack().isEmpty())
            return;
        int size = 100;
        if(!provider.getName().equals(Provider.PROVIDER_GPS))
            size = 1000;
        for(int i = 0; i < (provider.getTrack().size()/* >= size ? size : provider.getTrack().size()*/); i++){
            Location loc = new Location(provider.getName());
            loc.setLatitude(provider.getTrack().get(i).getLat());
            loc.setLongitude(provider.getTrack().get(i).getLon());
            /*if(provider.getName().equals(Provider.PROVIDER_GPS))
                track.getGPS().addTrack(provider.getTrack().get(i));
            if(provider.getName().equals(Provider.PROVIDER_KALMAN))
                track.getKalman().addTrack(provider.getTrack().get(i));
            if(provider.getName().equals(Provider.PROVIDER_GEOHASH))
                track.getGeoHash().addTrack(provider.getTrack().get(i));*/
            onLocationChanged(loc, provider.getName());
        }
    }

    private void filtered(GeoPoint point){

    }

    @Override
    public void setLocation(Location location, String provider) {
        if(mMap == null)
            return;

        onLocationChanged(location, provider);
    }

    @Override
    public void saveToFile() {
        if(FileManager.saveToFile(mGson.toJson(mTrack)))
            mTrack = new Track();
    }

    @Override
    public void changeCheckBox(boolean[] provider) {
        mPolylineGPS.setVisible(provider[0]);
        mPolylineKalman.setVisible(provider[1]);
        mPolylineKalmanGeoHash.setVisible(provider[2]);

        Log.e(LOG_TAG, "GPS size: "+mPolylineGPS.getPoints().size()+"; Kalman size: "+mPolylineKalman.getPoints().size()+"; GeoHash: "+mPolylineKalmanGeoHash.getPoints().size());
    }
}
