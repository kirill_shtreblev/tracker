package com.shtreb.tracker.cases;

import android.hardware.GeomagneticField;
import android.hardware.SensorManager;
import android.location.Location;
import android.util.Log;

import com.shtreb.tracker.cases.interfaces.GPSLocationListener;
import com.shtreb.tracker.cases.interfaces.SensorLocationListener;
import com.shtreb.tracker.entities.GeoPoint;
import com.shtreb.tracker.entities.SensorGpsDataItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;

public class LocationListener implements GPSLocationListener, SensorLocationListener, com.shtreb.tracker.cases.interfaces.LocationListener.Initializer {
    public static final String LOG_TAG = LocationListener.class.getName();

    public static final String PROVIDER_GPS = "GPS";
    public static final String PROVIDER_KALMAN = "Kalman";
    public static final String PROVIDER_KALMAN_GEOHASH = "GeoHash";

    private com.shtreb.tracker.cases.interfaces.LocationListener.CallBack mCallBack;
    private GPSLocation mGPSLocation;
    private SensorLocation mSensorLocation;

    private KalmanFilter mKalmanFilter;
    private GeoHashRTFilter mGeoHashFilter;

    private Subscription mTimer;

    private Queue<SensorGpsDataItem> mSensorDataQueue = new PriorityBlockingQueue<>();

    private double mMagneticAcc = 0;

    private Location mLastLocation;

    public LocationListener(com.shtreb.tracker.cases.interfaces.LocationListener.CallBack callBack, SensorManager sManager){
        mCallBack = callBack;
        mGeoHashFilter = new GeoHashRTFilter(8, 2);

        init(sManager);
        createTimer();
    }

    private void init(SensorManager sManager){
        mGPSLocation = new GPSLocation(this);
        mSensorLocation = new SensorLocation(this, sManager);
    }

    private void createTimer(){
        mTimer = Observable.interval(500, TimeUnit.MILLISECONDS)
                .subscribeOn(AppSchedulers.io())
                .observeOn(AppSchedulers.mainThread())
                .subscribe(new Subscriber<Long>() {
                    @Override
                    public void onCompleted() {}

                    @Override
                    public void onError(Throwable e) {}

                    @Override
                    public void onNext(Long tripDuration) {
                        try {
                            SensorGpsDataItem sdi;
                            double lastTimeStamp = 0.0;

                            while ((sdi = mSensorDataQueue.poll()) != null) {
                                if (sdi.getTimestamp() < lastTimeStamp)
                                    continue;

                                lastTimeStamp = sdi.getTimestamp();

                                if (sdi.getGpsLat() == SensorGpsDataItem.NOT_INITIALIZED) {
                                    handlePredict(sdi);
                                    /*if (mLastLocation != null) {
                                        sdi = new SensorGpsDataItem(
                                                sdi.getTimestamp(),
                                                mLastLocation.getLatitude(),
                                                mLastLocation.getLongitude(),
                                                mLastLocation.getAltitude(),
                                                sdi.getAbsNorthAcc(),
                                                sdi.getAbsEastAcc(),
                                                sdi.getAbsUpAcc(),
                                                mLastLocation.getSpeed(),
                                                mLastLocation.getBearing(),
                                                mLastLocation.getAccuracy(),
                                                mLastLocation.getAccuracy() * 0.1,
                                                mMagneticAcc);
                                        handleUpdate(sdi);
                                    }*/
                                    //handleUpdate(sdi);
                                } else {
                                    handleUpdate(sdi);
                                }

                                //Log.e(LOG_TAG, "lat: "+mLastLocation.getLatitude()+"; lon: "+mLastLocation.getLongitude()+"; speed: "+mLastLocation.getSpeed());

                                mLastLocation = locationAfterUpdateStep(sdi);
                                mCallBack.onLocationChange(mLastLocation, PROVIDER_KALMAN);

                            }
                        }catch (Exception e){e.printStackTrace();}
                    }
                });
    }

    private void handlePredict(SensorGpsDataItem sdi) {
        mKalmanFilter.predict(sdi.getTimestamp(), sdi.getAbsEastAcc(), sdi.getAbsNorthAcc());
    }

    private void handleUpdate(SensorGpsDataItem sdi) {
        mKalmanFilter.update(
                sdi.getTimestamp(),
                Coordinates.longitudeToMeters(sdi.getGpsLon()),
                Coordinates.latitudeToMeters(sdi.getGpsLat()),
                sdi.getSpeed()*Math.cos(sdi.getCourse()),
                sdi.getSpeed()*Math.sin(sdi.getCourse()),
                sdi.getPosErr(),
                sdi.getVelErr()
        );
    }

    private Location locationAfterUpdateStep(SensorGpsDataItem sdi) {
        double xVel, yVel;
        Location loc = mGPSLocation.getLastLocation();
        GeoPoint pp = Coordinates.metersToGeoPoint(mKalmanFilter.getCurrentX(),
                mKalmanFilter.getCurrentY());
        loc.setLatitude(pp.Latitude);
        loc.setLongitude(pp.Longitude);
        loc.setAltitude(sdi.getGpsAlt());
        xVel = mKalmanFilter.getCurrentXVel();
        yVel = mKalmanFilter.getCurrentYVel();
        double speed = Math.sqrt(xVel*xVel + yVel*yVel); //scalar speed without bearing
        loc.setBearing((float)sdi.getCourse());
        loc.setSpeed((float) speed);
        loc.setTime(System.currentTimeMillis());
        loc.setElapsedRealtimeNanos(System.nanoTime());
        loc.setAccuracy((float) sdi.getPosErr());

        if (mGeoHashFilter != null) {
            mGeoHashFilter.filter(loc);
            if(!mGeoHashFilter.getGeoFilteredTrack().isEmpty())
                mCallBack.onLocationChange(mGeoHashFilter.getmCurrentLocation(), PROVIDER_KALMAN_GEOHASH);
        }

        return loc;
    }

    private void stopTimer(){
        if(mTimer != null)
            mTimer.unsubscribe();
    }

    @Override
    public void onGPSLocationChange(Location location) {

        mCallBack.onLocationChange(location, PROVIDER_GPS);

        mMagneticAcc = new GeomagneticField(
                (float) location.getLatitude(),
                (float) location.getLongitude(),
                (float) location.getAltitude(),
                location.getElapsedRealtimeNanos()).getDeclination();

        if(mKalmanFilter == null) {
            createKalmanFilter(location);
            return;
        }

        mSensorDataQueue.add(new SensorGpsDataItem(
                location.getTime(), location.getLatitude(), location.getLongitude(), location.getAltitude(),
                SensorGpsDataItem.NOT_INITIALIZED,
                SensorGpsDataItem.NOT_INITIALIZED,
                SensorGpsDataItem.NOT_INITIALIZED,
                location.getSpeed(),
                location.getBearing(),
                location.getAccuracy(),
                location.getAccuracy()*0.1,
                mMagneticAcc));
    }

    @Override
    public void changeLinearAcceleration(float[] acceleration, int azimut, long time) {
        if(mKalmanFilter == null)
            return;

        //if(mLastLocation == null || mLastLocation.getLatitude() == 0 || mLastLocation.getLongitude() == 0){
            mSensorDataQueue.add(new SensorGpsDataItem(
                    time,
                    SensorGpsDataItem.NOT_INITIALIZED,
                    SensorGpsDataItem.NOT_INITIALIZED,
                    SensorGpsDataItem.NOT_INITIALIZED,
                    acceleration[1],
                    acceleration[0],
                    acceleration[2],
                    SensorGpsDataItem.NOT_INITIALIZED,
                    SensorGpsDataItem.NOT_INITIALIZED,
                    SensorGpsDataItem.NOT_INITIALIZED,
                    SensorGpsDataItem.NOT_INITIALIZED,
                    SensorGpsDataItem.NOT_INITIALIZED));
        /*    return;
        }

        double delta = 0;
        try{
            delta = (double)(time - mLastLocation.getTime())/1000;
        }catch (Exception e){
            return;
        }
        double x, y, xVel, yVel, speed;

        x = mKalmanFilter.getCurrentX();
        y = mKalmanFilter.getCurrentY();
        xVel = mKalmanFilter.getCurrentXVel()+(double)acceleration[0]*delta;
        yVel = mKalmanFilter.getCurrentYVel()+(double)acceleration[1]*delta;

        speed = Math.sqrt(Math.pow(xVel, 2)+Math.pow(yVel, 2));

        x += xVel*delta;
        y += yVel*delta;

        GeoPoint geo = Coordinates.metersToGeoPoint(x, y);
        mSensorDataQueue.add(new SensorGpsDataItem(
                time,
                geo.Latitude,
                geo.Longitude,
                SensorGpsDataItem.NOT_INITIALIZED,
                acceleration[1],
                acceleration[0],
                acceleration[2],
                speed,
                azimut,
                SensorGpsDataItem.NOT_INITIALIZED,
                SensorGpsDataItem.NOT_INITIALIZED,
                mMagneticAcc));

        /*mSensorDataQueue.add(new SensorGpsDataItem(
                time,
                SensorGpsDataItem.NOT_INITIALIZED,
                SensorGpsDataItem.NOT_INITIALIZED,
                SensorGpsDataItem.NOT_INITIALIZED,
                acceleration[1],
                acceleration[0],
                acceleration[2],
                speed,
                azimut,
                SensorGpsDataItem.NOT_INITIALIZED,
                SensorGpsDataItem.NOT_INITIALIZED,
                SensorGpsDataItem.NOT_INITIALIZED));*/
    }

    private void createKalmanFilter(Location location){
        mKalmanFilter = new KalmanFilter(
                false,
                Coordinates.longitudeToMeters(location.getLongitude()),
                Coordinates.latitudeToMeters(location.getLatitude()),
                location.getSpeed()*Math.cos(location.getBearing()),
                location.getSpeed()*Math.sin(location.getBearing()),
                0.1,
                location.getAccuracy(),
                location.getTime());
    }

    @Override
    public GPSLocation onLoadGPSLocation() {
        return mGPSLocation;
    }
}
