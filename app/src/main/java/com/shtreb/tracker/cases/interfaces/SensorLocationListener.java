package com.shtreb.tracker.cases.interfaces;

public interface SensorLocationListener {
    void changeLinearAcceleration(float[] acceleration, int azimut, long time);
}
