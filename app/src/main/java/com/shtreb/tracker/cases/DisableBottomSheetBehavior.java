package com.shtreb.tracker.cases;

import android.content.Context;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class DisableBottomSheetBehavior<V extends View> extends BottomSheetBehavior<V> {
    private boolean mAllowUserDragging;

    public DisableBottomSheetBehavior() {
        mAllowUserDragging = true;
    }

    public DisableBottomSheetBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
        mAllowUserDragging = true;
    }

    public void setAllowUserDragging(boolean allowUserDragging) {
        mAllowUserDragging = allowUserDragging;
    }

    public boolean onInterceptTouchEvent(CoordinatorLayout parent, V child, MotionEvent event) {
        if (mAllowUserDragging) {
            return super.onInterceptTouchEvent(parent, child, event);
        }
        return false;
    }
}
