package com.shtreb.tracker.cases.interfaces;

import android.location.Location;

import com.shtreb.tracker.cases.GPSLocation;

public interface LocationListener {
    public interface CallBack {
        void onLocationChange(Location location, String provider);
    }

    public interface Initializer {
        GPSLocation onLoadGPSLocation();
    }
}
