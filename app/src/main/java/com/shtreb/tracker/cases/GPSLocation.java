package com.shtreb.tracker.cases;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import com.shtreb.tracker.cases.interfaces.GPSLocationListener;

public class GPSLocation implements LocationListener{
    public static final String LOG_TAG = GPSLocation.class.getName();

    private GPSLocationListener mCallBack;
    private Location mLocation;

    public GPSLocation(GPSLocationListener callBack) {
        this.mCallBack = callBack;
    }

    @Override
    public void onLocationChanged(Location location) {
        mLocation = location;
        mCallBack.onGPSLocationChange(location);
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    public Location getLastLocation(){
        return mLocation;
    }
}
