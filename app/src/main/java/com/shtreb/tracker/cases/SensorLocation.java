package com.shtreb.tracker.cases;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.opengl.Matrix;

import com.shtreb.tracker.cases.interfaces.SensorLocationListener;

public class SensorLocation implements SensorEventListener {
    public static final String LOG_TAG = SensorLocation.class.getName();

    private SensorLocationListener mCallBack;
    private SensorManager mManager;

    private Sensor mSensorAcc, mSensorOri, mSensorDefAcc, mSensorMag;
    private float[] mLinAcc = new float[4],
        mRotationMatrix = new float[16],
        mInvRotationMatrix = new float[16],
        mAbsAcceleration = new float[4],
        mDefAcceleration = new float[3],
        mSimpleRotationMatrix = new float[9],
        mAngleMatrix = new float[3];

    private int mAzimut = 0;

    public SensorLocation(SensorLocationListener callBack, SensorManager sManager) {
        mCallBack = callBack;
        this.mManager = sManager;

        init();
    }

    private void init(){
        if(mManager == null)
            return;

        mSensorAcc = mManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        mSensorOri = mManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        mSensorDefAcc = mManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorMag = mManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        mManager.registerListener(this, mSensorAcc, SensorManager.SENSOR_DELAY_NORMAL);
        mManager.registerListener(this, mSensorOri, SensorManager.SENSOR_DELAY_NORMAL);
        mManager.registerListener(this, mSensorDefAcc, SensorManager.SENSOR_DELAY_NORMAL);
        mManager.registerListener(this, mSensorMag, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent e) {
        switch (e.sensor.getStringType()){
            case Sensor.STRING_TYPE_LINEAR_ACCELERATION:
                System.arraycopy(e.values, 0, mLinAcc, 0, 3);
                filterAcl();
                Matrix.multiplyMV(mAbsAcceleration, 0, mInvRotationMatrix, 0, mLinAcc, 0);
                mCallBack.changeLinearAcceleration(mAbsAcceleration, mAzimut, System.currentTimeMillis());
                break;
            case Sensor.STRING_TYPE_ROTATION_VECTOR:
                SensorManager.getRotationMatrixFromVector(mRotationMatrix, e.values);
                Matrix.invertM(mInvRotationMatrix, 0, mRotationMatrix, 0);
                break;
            case Sensor.STRING_TYPE_ACCELEROMETER:
                System.arraycopy(e.values, 0, mDefAcceleration, 0, e.values.length);
                break;
            case Sensor.STRING_TYPE_MAGNETIC_FIELD:

                SensorManager.getRotationMatrix(mSimpleRotationMatrix, null, mDefAcceleration, e.values);
                SensorManager.getOrientation(mSimpleRotationMatrix, mAngleMatrix);

                mAzimut = (int) (mAngleMatrix[0]*180/Math.PI);
                if(mAzimut < 0) mAzimut += 360;
                break;
            default:
                break;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {}

    private void filterAcl(){
        for(int i = 0; i < mAbsAcceleration.length; i ++){
            if(Math.abs(mAbsAcceleration[i]) < mSensorAcc.getResolution()) {
                mAbsAcceleration[i] = 0;
            }else if(mAbsAcceleration[i] > 0 && mAbsAcceleration[i] > mSensorAcc.getResolution()){
                mAbsAcceleration[i] -= mSensorAcc.getResolution();
            }else if(mAbsAcceleration[i] < 0 && Math.abs(mAbsAcceleration[i]) > mSensorAcc.getResolution()){
                mAbsAcceleration[i] += mSensorAcc.getResolution();
            }
        }
    }
}
