package com.shtreb.tracker.cases.interfaces;

import android.location.Location;

public interface GPSLocationListener {
    void onGPSLocationChange(Location location);
}
