package com.shtreb.tracker.cases;

import android.os.Environment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

public class FileManager {
    public static final String LOG_TAG = FileManager.class.getName();

    public static final String FILE_NAME = "LOG_";
    public static final String FILE_PATH = Environment.getExternalStorageDirectory().getPath()+"/Download/";

    public static boolean saveToFile(String string){
        try {
            File file = new File(FILE_PATH, FILE_NAME+System.currentTimeMillis());
            FileOutputStream out = new FileOutputStream(file);
            out.write(string.getBytes());
            out.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    //root 1527608155181
    //
    public static String readLogFile(){
        try {
            File file = new File(FILE_PATH, FILE_NAME + "1527608155181");
            FileInputStream in = new FileInputStream(file);
            BufferedReader bw = new BufferedReader(new InputStreamReader(in));
            String buff;
            StringBuilder res = new StringBuilder();
            while((buff = bw.readLine()) != null)
                res.append(buff);
            bw.close();
            return res.toString();
        }catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }
}
